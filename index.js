
let conteudoLetras = document.getElementById("letters-div");
let conteudoPalavras = document.getElementById("words-div");
let botao = document.getElementById('count-button');

botao.onclick = function(){
    contaLetras();
    contaPalavras();
}

function contaLetras(){
    conteudoLetras.innerHTML="";
    let span = document.createElement("span");
    let contadorLetras = [];
    let texto = normalizarTexto();
    texto = texto.replace(/[^a-z']+/g, ""); 
    for(let i = 0; i<texto.length;i++){
        let letraAtual;
        letraAtual = texto[i];
        if (contadorLetras[letraAtual]===undefined){
         contadorLetras[letraAtual]=1;
        }
        else{
         contadorLetras[letraAtual]++;
        }      
    }

    for (let letra in contadorLetras) {  
        const textContent = document.createTextNode('"' + letra + "\": " + contadorLetras[letra] + ", "); 
        span.appendChild(textContent); 
        conteudoLetras.appendChild(span); 
     }  
}

function contaPalavras(){
    let span = document.createElement("span");
    let contadorPalavras=[];
    let texto = normalizarTexto();
    let palavras = texto.split(/\s/);
    for(let i=0;i < palavras.length;i++){
        if(contadorPalavras[palavras[i]]===undefined){
            contadorPalavras[palavras[i]]=1
        }
        else{
            contadorPalavras[palavras[i]]++;
        }
    }
    for(let palavra in contadorPalavras){
        const textContent = document.createTextNode('"'+ palavra + "\": "+contadorPalavras[palavra]+', ');
        span.appendChild(textContent);
        conteudoPalavras.appendChild(span);
    }
    console.log(contadorPalavras)
}
function normalizarTexto(){
    let texto = document.getElementById('texto').value;    
    texto = texto.toLowerCase();
    texto = texto.replace(/[^a-z'\s]+/g, ""); 
    return texto;
}
